public aspect WalletAspect {
	pointcut safeWithdraw() : call(* Wallet.safeWithdraw(int));

	before(int price) : safeWithdraw() && args(price){
	  try {
		Wallet wallet = new Wallet();
		if ( wallet.getBalance() < price)
			System.err.println("Warning, you do not have enough to purchase.");
	} 
	   catch (Exception e) {
		System.out.println("Exception Caught");
	}
	}
	after(int price) returning (int withdrawnAmount): safeWithdraw() && args(price) {
   	   try {
		Wallet wallet = new Wallet();
		if (wallet.getBalance() == 0) {
			wallet.safeDeposit(withdrawnAmount);
			System.err.println("Warning, the withrdawn value is less than the price. The money was placed back into your wallet.");
			
		}
	}
		catch (Exception e) {
			System.out.println("Exception caught");
		}

	}
}
