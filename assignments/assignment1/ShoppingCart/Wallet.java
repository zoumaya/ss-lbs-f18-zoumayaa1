import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Wallet {
   /**
    * The RandomAccessFile of the wallet file
    */  
   private RandomAccessFile file;

   /**
    * Creates a Wallet object
    *
    * A Wallet object interfaces with the wallet RandomAccessFile
    */
    public Wallet () throws Exception {
	this.file = new RandomAccessFile(new File("wallet.txt"), "rw");
    }

   /**
    * Gets the wallet balance. 
    *
    * @return                   The content of the wallet file as an integer
    */
    public int getBalance() throws IOException {
	this.file.seek(0);
	return Integer.parseInt(this.file.readLine());
    }

   /**
    * Sets a new balance in the wallet
    *
    * @param  newBalance          new balance to write in the wallet
    */
    public synchronized void setBalance(int newBalance) throws Exception {
	this.file.setLength(0);
	String str = new Integer(newBalance).toString()+'\n'; 
	this.file.writeBytes(str); 
    }
    
    public synchronized int safeWithdraw(int valueToWithdraw) throws Exception {
	try {

		this.file = new RandomAccessFile(new File("wallet.txt"), "rw");
		int balance = getBalance();
		if (valueToWithdraw > balance) 
			setBalance(0);
		else 
			setBalance(balance - valueToWithdraw);
		return getBalance();
	}
	catch (Exception e) {
		System.out.print("Exception Caught");
	}  
	return 0; 
    }

    public synchronized  void safeDeposit(int valueToDeposit) throws Exception {
	try { 
		int balance = getBalance();
		setBalance(valueToDeposit+balance);
	}
	catch (Exception e) {
		System.out.print("Exception Caught");
	}
    }		
   /**
    * Closes the RandomAccessFile in this.file
    */
    public void close() throws Exception {
	this.file.close();
    }
}
