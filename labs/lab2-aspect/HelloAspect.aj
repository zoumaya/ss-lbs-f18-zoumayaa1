import java.util.Date;

public aspect HelloAspect{
	pointcut greeting(): call(* Hello.greeting(..));
	before(): greeting(){
          System.out.println("AOP test from Zoumaya! This is before Hello.greeting(...) is called.");    
    	}
	after(): greeting() {
		System.out.println("This is after the Hello.greeting(...) invoked. The time is now: " + (new Date()).toString());		
	}
}
