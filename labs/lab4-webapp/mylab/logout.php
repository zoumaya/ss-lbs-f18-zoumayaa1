<!DOCTYPE html>
<html>
<head>
	<title>Simple Web Application - Lab 4 - CPS499/592-02 F2018</title>
</head>
<body>
	<h1>CPS499/592-02 F2018 - Lab 4</h1>
	<h2> A Simple Web Application</h2> 
   	<h2>Simple logout page by <font color="blue">Phu Phung</font>, customized by "Alex Zoumaya"</h2>
<?php 
	session_start();
	session_destroy();
	echo "Current time: " . date("Y-m-d h:i:sa") . "<br>\n";
?>
	<p>You are logged out. Please click <a href="login.php">here</a> to login again.</p>
</body>
</html>

